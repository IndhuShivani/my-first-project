def get_inputs():
  Ndays, Kterms = [int(i) for i in input().split()]
  given = [i for i in input()]
  return Ndays, given, Kterms

def Nconsecutive_ones(given, Ndays):
  seq = [i for i in given]
  count = 0  
  result = 0 
  for i in range(0, Ndays):
    if seq[i] == '0':
      count = 0
    count += 1 
    result = max(result, count)  
  return result - 1

def max_pizza_time():
  Ndays, given, Kterms = get_inputs()
  seq = [i for i in given]
  changes = []
  for i in range(Ndays - 1):
    for j in range(Kterms - 1):
      seq[i+j] = '1'
    changes.append(Nconsecutive_ones("".join(seq),Ndays))
    seq = [i for i in given]
  return max(changes)

print(max_pizza_time())
