def golf_terms(par_diff):
  if par_diff == -4:
   return "condor"
  if par_diff == -3:
   return "albatross"
  if par_diff == -2:
   return "eagle"
  if par_diff == -1:
   return "birdie"
  if par_diff == 1:
   return "bogey"
  if par_diff == 2:
   return "double-bogey"
  if par_diff == 3:
   return "triple-bogey"
  return "par"

def get_inputs():
  Nholes = int(input())
  exp_pars = [int(i) for i in input().split()]
  player_pars = [int(i) for i in input().split()]
  return Nholes, sum(exp_pars), sum(player_pars)

def final_score():
  Nholes, par_score, golfer_score = get_inputs()
  return golf_terms(golfer_score - par_score)

print(final_score())
