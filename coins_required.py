def how_many_nodes():
  Nnodes,positions = get_inputs()
  no_root = [i for i in positions if i[0] != 1]
  common_nodes = [no_root[i] for i in range(len(no_root) - 1) if no_root[i][0] == no_root[i+1][0]]
  return Nnodes - len(common_nodes) 
 
def get_inputs():
  Nnodes = int(input())
  positions = [ [int(i) for i in input().split()] for x in range(Nnodes - 1)]
  return Nnodes,positions

def coins_required():
  Tcases = int(input())
  return [how_many_nodes() for i in range(Tcases)]

print(coins_required())

  
