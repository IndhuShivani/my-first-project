def no_of_houses(Nrows,nH_rivers,H_rivers,dimension):
  nHouses = 0
  const = 0
  for i in range(nH_rivers):
    nHouses += (H_rivers[i]-const-1)//dimension
    const = H_rivers[i]
  nHouses += (Nrows-const)//dimension
  return nHouses
nhouses = []
for i in range(int(input())):
  Nrows,Mcols = [int(i) for i in input().split()]
  nH_rivers,nV_rivers,dimension = [int(i) for i in input().split()]
  H_rivers = [int(i) for i in input().split()]
  V_rivers = [int(i) for i in input().split()]
  nhouses.append((no_of_houses(Nrows,nH_rivers,H_rivers,dimension) * no_of_houses(Mcols,nV_rivers,V_rivers,dimension)))
for i in nhouses:
  print(i)
