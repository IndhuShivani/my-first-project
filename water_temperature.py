def is_possible():
  v1,t1,v2,t2,v3,t3 = [int(i) for i in input().split()] 
  t = (v1 * t1 + v2 * t2) // (v1 + v2)
  if t3 == t2 or t3 == t1 or t3 <= t:
    return "YES"
  return "NO"

def water_temperature():
  n = int(input())
  outputs = []
  for i in range(n):
    outputs.append(is_possible())
  return outputs

for i in water_temperature():
  print(i)
