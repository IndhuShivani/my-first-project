def get_inputs():
  N,S,K = input().split()
  A = [input().split() for i in range(int(N))]
  return N,S,K,A

def is_prime(n):
  if n in [2,3,5,7]:
    return True
  if n % 2 == 0:
    return False
  r = 3
  while r * r <= n:
    if n % r == 0:
      return False
    r += 2
  return True

def prime_factors(n):
  return [i for i in range(2,int(math.sqrt(n))) if n % i == 0 and if is_prime(i)]

def special_prime_list():
  N,S,K,A = get_inputs()
  return is_prime(int(N) * (int(K) - int(S) - max([prime_factors(i) for i in A])))

print(special_prime_list())
