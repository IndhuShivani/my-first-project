def baseb_to_decimal(n,b):
  num = [int(x) for x in str(n)]
  return int(sum([pow(b,i) * num[::-1][i] for i in range(len(num))]))

def XOR(num1,num2):
  if num1 != num2:
    return 1
  return 0

def n_bit_XOR(bs1,bs2):
  num1 = [int(i) for i in str(bs1)]
  num2 = [int(i) for i in str(bs2)]
  output = []
  for i in range(len(num1)):
    output.append(XOR(num1[i],num2[i]))
  return int("".join(map(str, output))) 

def convert_to_n_bit(bs,n):
  num = [int(i) for i in str(bs)]
  n_bit_num = [0 for i in range(n-len(num))]
  output = n_bit_num + num
  return  "".join(map(str, output)) 

def get_inputs():
  given = input().split()
  LowerL,UpperL = [int(i) for i in input().split()]
  XOR_with_num = input()
  given = [convert_to_n_bit(i,len(XOR_with_num)) for i in given]
  return given,LowerL,UpperL,XOR_with_num

def binary_strings():
  given,LowerL,UpperL,XOR_with_num = get_inputs()
  output = [n_bit_XOR(given[i],XOR_with_num) for i in range(LowerL-1, UpperL)]
  return max([baseb_to_decimal(i,2) for i in output])

print(binary_strings())
