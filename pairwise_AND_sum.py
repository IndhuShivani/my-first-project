def get_inputs():
  Nlen = int(input())
  if Nlen <= 100:
    given = [int(i) for i in input().split()]
  return Nlen, given

def baseb_to_decimal(n,b):
  num = [int(x) for x in str(n)]
  return int(sum([pow(b,i) * num[::-1][i] for i in range(len(num))]))

def decimal_to_baseb(rem,n,b):
  if n >= 1:
    rem.append(str(n % b))
    decimal_to_baseb(rem , n // b , b)
  return int(''.join(rem[::-1]))

def AND(num1,num2):
  if num1 == 1 and num2 == 1:
    return 1
  return 0
  
def bitwise_AND(n1,n2):
  num1 = [int(i) for i in str(n1)]
  num2 = [int(i) for i in str(n2)]
  output = []
  for i in range(len(num1)):
    output.append(AND(num1[i],num2[i]))
  return int("".join(map(str, output))) 

def convert_to_n_bit(b_num,n_bits):
  num = [int(i) for i in str(b_num)]
  n_bit_zeros = [0 for i in range(n_bits-len(num))]
  n_bit_num = n_bit_zeros + num
  return  "".join(map(str, n_bit_num)) 

def pairwise_AND(num1,num2):
  bn1 = decimal_to_baseb([],num1,2)
  bn2 = decimal_to_baseb([],num2,2)
  n_bits = len([int(i) for i in str(bn2)])
  return baseb_to_decimal( bitwise_AND( convert_to_n_bit(bn1,n_bits) , bn2 ) , 2)

def pairwise_AND_sum():
  Nlen, given = get_inputs()
  return sum([pairwise_AND(i,j) for i in given for j in given if i < j])

print(pairwise_AND_sum())
