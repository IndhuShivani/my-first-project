from itertools import combinations as c

def get_inputs():
  N,K = input().split()
  return [int(K)**int(i) for i in input().split()] 

def subsequences():
  given = get_inputs()
  subseq = []
  for i in range(2,len(given)-1):
    subseq.append(list(c(given,i)))
  return subseq

def maximize_it():
  subseq = subsequences()
  psums =  [sum(x) for i in subseq for x in i]
  return [x for i in subseq for x in i if sum(x) == max(psums)]

print(maximize_it())
