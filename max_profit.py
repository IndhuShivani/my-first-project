def get_inputs(T):
  for i in range(T):
    planets = []
    Nplanets,Acoins = input().split()
    for i in range(int(Nplanets)):
      planets.append([int(i) for i in input().split()])
  return int(Nplanets),int(Acoins),planets

def max_profit(T):
  N,A,planets = get_inputs(T)
  prefixSum = [0]*N
  prefixSum[0] = planets[0][0]
  for i in range(1,N):
    prefixSum[i] = prefixSum[i-1] + planets[i][0]
  return N,A,planets,prefixSum

def earnings(T):
  N,A,planets,prefixSum = max_profit(T)
  maxProfit = 0
  for i in range(N):
    Max = planets[i][1]
    Min = planets[i][1]
    for j in range(i,N):
      if Max < planets[j][1]:
        Max = planets[j][1]
      elif Min > planets[j][1]:
        Min = planets[j][1]
        totalCost = A * (j-i+1) - (prefixSum[j] - prefixSum[i] + planets[i][0]) - (Max-Min)**2
        maxProfit = max(maxProfit,totalCost)
  return maxProfit
    
T = int(input())
print(earnings(T))
