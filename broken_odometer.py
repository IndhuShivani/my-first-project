def odometer():
  inputs = [5,14,76,67,40]
  return [distance_traveled(i) for i in inputs]
def is_three(n):
  return (list(str(n))[0] == '3' or list(str(n))[1] == '3' )
def distance_traveled(n):
  x = [i for i in range(1,n+1)]
  y = [i for i in range(1,n+1) if ( i > 9 and is_three(i) == False ) or (i <= 9 and i != 3)]
  return max(y) - (len(x) - len(y))
print(odometer())

