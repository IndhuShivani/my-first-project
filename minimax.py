def setup():
  N = int(input())
  board = []
  for i in range(N):
    board.append([int(i) for i in input().strip().split()])
  return N,board

def is_nice(N,board):
  min_rows = [min(board[i]) for i in range(N)]
  max_cols = [max(i) for i in zip(*board)]
  return max(min_rows) == min(max_cols)

def minimax():
  N,board = setup()
  if is_nice(N,board):
    return "Yes"
  return "No"

print(minimax())
