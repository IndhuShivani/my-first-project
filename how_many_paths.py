def NxN_squaregrid():
  N = int(input())
  return N,[[i for i in input().split()] for i in range(N)]

def if_any_obstacle(N,grid,paths):
  if grid[0][0] == "_":
   paths[0][0] = 1
  for i in range(1,N):
   if grid[i][0] == "_":
    paths[i][0] = paths[i-1][0]
  for j in range(1,N):
   if grid[0][j] == "_":
    paths[0][j] = paths[0][j-1]
  return paths

def how_many_paths():
  N,grid = NxN_squaregrid()
  paths = [[0]*N for i in grid]
  paths = if_any_obstacle(N,grid,paths)
  for i in range(1,N):
   for j in range(1,N):
     if grid[i][j] == "_":
       paths[i][j] = paths[i-1][j] + paths[i][j-1]
  return paths[-1][-1]

print(how_many_paths())
