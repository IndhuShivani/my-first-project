def get_inputs():
  num = int(input())
  stamps = [int(i) for i in input().split()]
  return num, sum(stamps)

def can_divide():
 num, total_stamps = get_inputs()
 if total_stamps == int(num * (num + 1) / 2):
   return "YES"
 return "NO"

print(can_divide())
