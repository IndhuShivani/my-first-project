def the_best_box():
  Tcases = int(input())
  volumes = []
  for i in range(Tcases):
   wire_len , paper_area = map(int,input().split())
   volumes.append(largest_volume(wire_len , paper_area))
  return volumes
  
def largest_volume(wire_len , paper_area):
  root = ((wire_len * wire_len) - (24 * paper_area)) ** 0.5
  vp = (paper_area - root) / 12
  volume = (paper_area * vp / 2) - (wire_len * vp * vp / 4) + (vp * vp * vp)
  return round(volume,2)

print(the_best_box())
