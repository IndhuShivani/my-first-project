def get_inputs():
  Njewels,Mdays = input().strip().split()
  jewels_values = [int(i) for i in input().split()]
  limits = [[int(i) for i in input().split()] for i in range(int(Mdays))]
  return jewels_values,limits

def Mth_day():
  jewels_values,limits = get_inputs()
  stolen_jewels = []
  for l in limits:
    stolen_jewels.append(sum([jewels_values[i] for i in range(l[0]-1, l[1])]))
  Mth_day = [i + 1 for i in range(len(stolen_jewels)) if stolen_jewels[i] == max(stolen_jewels)]
  return int("".join(map(str, Mth_day)))

print(Mth_day())
