def get_inputs():
  N,M,Z,L,R,B = map(int,input().strip().split())
  return N,M,Z,L,R,B
def seating_arrangement():
   N,M,Z,L,R,B = get_inputs()
   return min(N * M, L + R + Z + min(B, N * (M + 1) - L - R >> 1, N * (M +1 >> 1)))
print(seating_arrangement())
