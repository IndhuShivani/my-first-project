from bisect import bisect_left as bl

def get_inputs():
  Nlen,Qqueries = map(int,input().split())
    given = input()
  return Nlen, Qqueries,given

def is_rich_sub_string():
    Nlen,Queries,given = get_inputs()
    p = []
    for i in range(len(given)-2):
      if given[i] == given[i+1] or given[i+1] == given[i+2] or given[i] == given[i+2]:
        p.append(i+1)
    for i in range(Qqueries):
      l,r = map(int,input().split())
      freq = bl(p,l)
      if len(p) == 0 or (r-l) < 2 or freq == len(p):
        return "NO"
      if p[freq] + 2 <= r:
        return "YES"
      return "NO"

T = int(input())
for i in range(T):
  print(is_rich_sub_string())
