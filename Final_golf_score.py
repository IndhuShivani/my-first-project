def values(pars):
  golf_terms = ["par", "bogey", "double-bogey", "triple-bogey", "albatross", "eagle", "birdie"]
  return [x - len(golf_terms) if x > 4 else x for x in [golf_terms.index(i) for i in pars]]

def get_inputs():
  exp_pars = [int(i) for i in input().split()]
  golfer_pars = input().split()
  return exp_pars, golfer_pars 

def final_golf_score():
  exp_pars, golfer_pars = get_inputs()
  score = sum(values(golfer_pars))
  if score < 0:
    return str(score) + " " + "under par"
  if score > 0:
    return str(score) + " " + "over par"
  return "par"
  
print(final_golf_score(exp_pars, golfer_pars))
