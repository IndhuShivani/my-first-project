def baseb_to_decimal(n,b):
  num = [int(x) for x in str(n)]
  return int(sum([pow(b,i) * num[::-1][i] for i in range(len(num))]))

def feedback():
  num = input()
  if baseb_to_decimal(num,2) > 5 :
    return "good"
  return "bad"

print(feedback())
