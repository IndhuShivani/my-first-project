from itertools import combinations as ncr

def get_inputs():
  Nnotes, Dmoney = [int(i) for i in input().split()]
  notes = [int(i) for i in input().split()]
  return Nnotes, Dmoney, notes

def can_pay():
  Nnotes, Dmoney, notes = get_inputs()
  for i in range(2, Nnotes):
    if Dmoney in [sum(k) for k in list(ncr(notes,i))]:
      return "Yes"
  return "No"

print(can_pay())
