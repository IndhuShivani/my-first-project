def is_equal(seq,Ksum):
  return any([True if i != j and i+j == K else False for i in A for j in A ])

def chef_and_sum(T):
  is_possible = []
  for i in range(T):
    Nvalues,Ksum = input().split()
    seq = [int(i) for i in input().split()]
    if is_equal(seq,int(Ksum)) == True:
      is_possible.append("Yes")
    is_possible.append("No")
  return is_possible

T = int(input())
for i in chef_and_sum(T):
  print(i)
