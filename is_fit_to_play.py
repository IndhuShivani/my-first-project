def get_inputs():
  Nlen = int(input())
  seq = [int(i) for i in input().split()]
  return seq

def is_fit():
  given = get_inputs()
  seq = [given[i+1]-given[i] for i in range(len(given)-1) if given[i+1]-given[i] > 0]
  if seq == []:
    return "UNFIT"
  return max(seq)

def fit_to_play():
  Tcases = int(input())
  outputs = []
  for i in range(Tcases):
    outputs.append(is_fit())
  return outputs

for i in fit_to_play():
  print(i)

