def get_inputs():
  topics, days, hours = map(int, input().split())
  times = map(int,input().split())
  times = sorted(times, reverse = True)
  return topics,days,hours,times

def num_of_topics():
  ntopics = 0
  ktopics = 0
  topics,days,hours,times = get_inputs()
  while n < days and len(times) > 0:
    each_time = times.pop()
    if each_time <= hours:
      ntopics += 1
      ktopics += 1
    if each_time > hours and each_time <= hours*2 and (days - ntopics) >= 2:
      ntopics += 2
      ktopics += 1
  return ktopics

print(num_of_topics())
