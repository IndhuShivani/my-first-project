from itertools import product as p
def get_inputs():
  N,M = input().split()
  seq = []
  for i in range(int(N)):
    seq.append(input().split())
  return N,M,seq
def gcd(x,y):
  while(y): 
    x, y = y, x % y 
  return x 
def GCD_sum():
  N,M,seq = get_inputs()
  return sum([gcd(int(x[i]),int(y[j]))  for x in seq for y in seq for i in range(int(M)) for j in range(int(M))])
print(GCD_sum())
