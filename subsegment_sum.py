import math

L = int(input())
A = [int(x) for x in input().split(" ")]
Q = int(input())

def least_frequent(A):
    values = dict()
    for x in A:
      values[x] = 0
    for x in A:
      values[x] += 1
    Count = len(A) + 1
    for key, value in values.items():
      if Count > value:
        Count = value
    return Count

def compute_sum(A):
    i = 0;
    start = 0;
    count = 0;
    while i < len(A) - 1:
      if math.fabs(A[i] - A[i + 1]) > 1:
        count += least_frequent(A[start:i + 1])
        start = i + 1
      i += 1
    count += least_frequent(A[start:])
    return count
    
def subsegment_sum(Q):
  answer = []
  while Q:
    query = list(input().split(" ")) 
    if int(query[0]) == 1:
        A[int(query[1])-1] = int(query[2])
    else:
      answer.append(compute_sum(sorted(A)))
    Q -= 1
  return answer
print(subsegment_sum(Q))
