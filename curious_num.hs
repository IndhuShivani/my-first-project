import Data.Char

factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n - 1)

toDigits :: Int -> [Int]
toDigits n = map digitToInt $ show n

isCurious :: Int -> Bool
isCurious n = (sum $ map factorial (toDigits n)) == n

main = do
  print(isCurious 145)

