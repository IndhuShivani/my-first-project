INPUTS = [5,14,76,67,40]
def odometer():
  return [distance_traveled(i) for i in INPUTS]
def is_containing_three(n):
  return '3' in list(str(n))
def distance_traveled(n):
   possible_numbers = [i for i in range(1,n+1) if is_containing_three(i) == False]
   return max(possible_numbers) - (n - len(possible_numbers))
print(odometer())
