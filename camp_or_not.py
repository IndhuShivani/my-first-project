def get_inputs(T):
  inputs = []
  queries = []
  for i in range(T):
    Ddays = int(input())
    for i in range(Ddays):
      d,p = input().strip().split()
      inputs.append((int(d),int(p)))
    nQueries = int(input())
    for i in range(nQueries):
      deadline,req = input().strip().split()
      queries.append((int(deadline),int(req)))
  return inputs,queries

def camp_or_not(T):
  inputs,queries = get_inputs(T)
  s = 0
  possible = []
  for i in range(len(inputs)):
    if inputs[i][0] <= queries[0]:
      s += inputs[i][1]
  if s >= queries[1]:
    possible.append("Go Camp")
  else:
    possible.append("Go Sleep")
  return possible

T = int(input())
for i in camp_or_not(T):
  print(i)
