def is_prime(n):
  if n in [2,3,5,7]:
    return True
  if n % 2 == 0:
    return False
  r = 3
  while r * r <= n:
    if n % r == 0:
      return False
    r += 2
  return True

def distinct_prime_factors(n):
  return [i for i in range(2,n+1) if n % i == 0 and is_prime(i) == True]

def how_many_K_primes():
  LowerL,UpperL,K_factors = [int(i) for i in input().split()]
  return len([i for i in range(LowerL ,UpperL + 1) if len(distinct_prime_factors(i)) >= K_factors])

def organize():
  Tcases = int(input())
  outputs = []
  for i in range(Tcases):
    outputs.append(how_many_K_primes())
  return outputs

print(organize())
