def minimum_difference(n):
    return min([abs(i-j) for i in range(1,n+1) for j in range(1,n+1) if i * j == n])

def arranging_cupcakes():
    Tcases = int(input())
    differences = []
    for i in range(Tcases):
        N_cakes = int(input())
        differences.append(minimum_difference(N_cakes))
    return differences

print(arranging_cupcakes())
