def divisors(n):
  return sum(list(i for i in range(1,n) if n % i == 0))
def Amicable_numbers(LIMIT):
  return list((i,j) for i in range(LIMIT) for j in range(LIMIT) if divisors(i) == j and divisors(j) == i and i > j)
print(Amicable_numbers(1000))
