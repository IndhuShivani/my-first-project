def side_of_triangle(x1,y1,x2,y2,x3,y3):
  a = ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5
  b = ((x3 - x2) ** 2 + (y3 - y2) ** 2) ** 0.5
  c = ((x1 - x3) ** 2 + (y1 - y3) ** 2) ** 0.5
  return a,b,c

def area_of_traingle(a,b,c):
  s = (a + b + c) / 2
  return ( s * (s - a) * (s - b) * (s - c) ) ** 0.5

def min_and_max_areas_at():
  Tcases = int(input())
  areas = []
  for i in range(Tcases):
    x1,y1,x2,y2,x3,y3 = [int(i) for i in input().split()]
    a,b,c = side_of_triangle(x1,y1,x2,y2,x3,y3)
    areas.append(area_of_traingle(a,b,c))
  return [i+1 for i in range(Tcases) if areas[i] == min(areas)] , [i+1 for i in range(Tcases) if areas[i] == max(areas)]

print(min_and_max_areas_at())
