def profit(val):
  return val - (n//2 + n//3 + n//4)

def factorial(n):
  if n == 0:
    return 1
  return n * factorial(n-1)

def cum_product_of_profit(coin_val):
  return factorial(profit(coin_val))
