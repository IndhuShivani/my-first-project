from itertools import permutations as npr

def get_input():
  n = int(input())
  s,nqueries = input().split()
  queries = [input().split() for i in range(int(nqueries))]
  queries = [(int(x[0]),int(x[1])) for x in queries]
  return n,s,queries

def gen_palindromes():
  n,s,queries = get_input()
  palindrome = []
  for L,R in queries:
    palindrome.append(s[:L-1] + make_palindrome(s[L-1:R]) + s[R:])
  return palindrome

def is_palindrome(s):
  return s == s[::-1]

def make_palindrome(s):
  palindromes = [''.join(p) for p in set(npr(s)) if is_palindrome(p)]
  return sorted(palindromes)[0]

print(gen_palindromes())
