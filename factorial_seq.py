def factorial(num):
  factorial = 1
  if num == 0:
   return 0
  for i in range(1, num + 1):
    factorial *= i
  return factorial

def factorial_seq(LIMIT):
  return [factorial(i) for i in range(1, LIMIT)]
