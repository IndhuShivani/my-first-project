def get_inputs():
  klen = int(input())
  return klen, [int(i) for i in input().split()], [i for i in range(1,klen+1)]

def factorial(n):
  count = 0
  i = 1
  r = n - 1
  while count < r:
    n = n * i
    r += 1
    count += 1
  return n

def ncr(n,r):
  return int((factorial(n) * factorial(r)) / factorial(n-1))

def summation_over_ncr(T):
  outputs = []
  for i in range(T):
    k,n,m = get_inputs()
    outputs.append(min([m[i] for i in range(k) if sum(ncr(n[i],m[i])) % 2 == 0]))
  return outputs

T = int(input())
print(summation_over_ncr(T))
