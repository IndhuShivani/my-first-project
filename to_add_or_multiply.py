def get_inputs():
  Nlen,who = input().strip().split()
  given = [int(i) for i in input().split()]
  return given,who

def add_or_multiply():
  given,who = get_inputs()
  if who == "chef":
    return sum([given[i] + given[i+1] for i in range(len(given)-1)])
  return sum([given[i] * given[i+1] for i in range(len(given)-1)])

def who_wins():
  num = add_or_multiply()
  if num % 2 == 0:
    return "chef"
  return "chefu"

print(who_wins())
