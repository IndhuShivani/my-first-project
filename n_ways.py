def get_inputs():
  N = int(input())
  return [int(i) for i in input().split()]

def valid_arrays(given):
  for i in range(len(given)-1,0,-1):
    if given[i] != -1 and given[i] != 1:
      before = given[i-1]
      must_be = given[i]-1
      if before == -1:
        given[i-1] = must_be
      if before != -1 and before != must_be:
        return 0
  if given[0] != -1 and given[0] != 1:
    return 0 
  return given

def n_ways(given):
  solved = valid_arrays(given)
  MOD = 10**9 + 7
  k = 0
  for i in range(1,len(solved)):
    if solved[i] == -1:
      k += 1 
  return pow(2,k,MOD)

def organize():
  T = int(input())
  outputs = []
  for i in range(T):
    given = get_inputs()
    outputs.append(n_ways(given))
  return outputs

print(organize())
