def number_of_moves(T,N,S):
  for i in range(T):
    deck1 = [i for i in range(1,N+1)]
    deck2 = list(reversed(deck1))
    M = 0
    while S != 0:
      deck2.append(deck2.pop(0))
      if deck1[0] == deck2[0]:
        S = S - 1
        deck2.pop(0)
        deck1.pop(0)
      M += 1
  return M
print(number_of_moves(1,3,1))
