def get_inputs():
  N_participants,R_scholarships,Xold_p,Yplagiarism_p = input().split()
  old_p = [input().split() for i in range(int(Xold_p))]
  plagiarism_p =  [input().split() for i in range(int(Yplagiarism_p))]
  return  N_participants,R_scholarships,old_p,plagiarism_p

def scholarship_eligibility():
  N,R,old_p,plagiarism_p = get_inputs()
  participants = [i for i in range(int(N))]
  non_eligible_p = set(old_p).union(plagiarism_p)
  return (len([participants.pop(int(i)) for i in non_eligible_p]) / N ) * 100

print(scholarship_eligibility())
