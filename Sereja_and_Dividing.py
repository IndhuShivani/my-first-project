def baseb_to_decimal(n,b):
  num = [int(x) for x in str(n)]
  return int(sum([pow(b,i) * num[::-1][i] for i in range(len(num))]))

def decimal_to_baseb(rem,n,b):
  if n >= 1:
    rem.append(str(n % b))
    decimal_to_baseb(rem , n // b , b)
  return int(''.join(rem[::-1]))

def convert_and_divide(x_ary):
  A,B,L = [int(i) for i in input().split()]
  Ad = baseb_to_decimal(A,x_ary)
  Bd = baseb_to_decimal(B,x_ary)
  rem = []
  return decimal_to_baseb(rem , int(Ad / Bd % pow(x_ary,L)) , x_ary)

def organize():
  outputs = []
  Tcases = int(input())
  if 1 <= Tcases and Tcases <= 10:
    for i in range(Tcases):
      outputs.append(convert_and_divide(7))
  return outputs

for i in organize():
  print(i)

