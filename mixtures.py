def get_inputs():
  Ncolors = int(input())
  mixture = [int(i) for i in input().split()]
  return Ncolors,mixture

def minimum_smoke():
  Ncolors,mixture = get_inputs()
  smoke = 0
  for i in range(len(mixture)-1):
    smoke += mixture[i] * mixture [i+1]
    mixture[i+1] = (mixture[i] + mixture [i+1]) % 100
  return smoke

print(minimum_smoke())
