def get_inputs():
  Ncities = int(input())
  cities = [int(i) for i in input().split()]
  Nqueries = int(input())
  queries = [[int(i) for i in input().split()] for i in range(Nqueries)]
  return Ncities, cities, Nqueries, queries

def total_enjoyment(cities):
  total_enj = 1
  for i in cities:
   total_enj *= i
  return total_enj

def first_query(p,f,cities):
  cities[p-1] = f
  return cities

def second_query(R,cities):
  return [cities[i] for i in range(0,len(cities),R)]

def chef_and_cities():
  Ncities, cities, Nqueries, queries = get_inputs()
  enjoyments = []
  for i in range(Nqueries):
   if queries[i][0] == 1:
    enjoyments.append(total_enjoyment(first_query(queries[i][1],queries[i][2],cities)))
   enjoyments.append(total_enjoyment(second_query(queries[i][1],cities)))
  return enjoyments

print(chef_and_cities())

