def get_inputs():
  Nhorses = int(input())
  skills = [int(i) for i in input().split()]
  return Nhorses , skills

def min_difference():
  Nhorses , skills = get_inputs()
  return min([abs(i-j) for i in skills for j in skills if i != j])

print(min_difference())
