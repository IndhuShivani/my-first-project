def get_inputs():
  return [int(i) for i in input().split()]
def can_connect_soliders():
  Nsoliders,Mmiles = get_inputs()
  Kspots = [i for i in range(1,Nsoliders+1)]
  return max([sum((i,Kspots[i],Nsoliders-i)) for i in range(Nsoliders)]) < Mmiles
print(can_connect_soliders())
