from itertools import permutations as p
def is_valid(s):
  return all([True if int(s[i]) % int(M) == i % int(M) else False for i in range(len(s))])
T = int(input())
for i in range(T):
  N,K,M = input().split()
  seq = input().strip().split()
  sub_seq = list(p("".join(seq),int(K))) 
  print(len([s for s in sub_seq if is_valid(s)]))
