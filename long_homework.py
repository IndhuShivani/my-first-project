def get_inputs():
  nlen,Qqueries,M = input().split()
  queries = []
  given = [int(i) for i in input().split()]
  for i in range(int(Qqueries)):
    queries.append(input().split())
  return given,queries,M

def nth_smallest(given,n):
  given.sort()
  return given[n-1]

def is_satisfied():
  given,queries,M = get_inputs()
  outputs = []
  for i in range(len(queries)):
    solved = set(j for j in range(len(given)) if given[j] % int(M) == int(queries[i][1]))
    outputs.append(nth_smallest(given,int(queries[0][i])))
  return outputs

print(is_satisfied()) 
