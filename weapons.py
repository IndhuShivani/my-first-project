def get_inputs():
  n = int(input())
  weapons = [0 for i in range(10)]
  for x in range(n):
    player = input()
  return weapons,player
  
def weapon_value(T):
  for i in range(T):
    weapons,player = get_inputs()
    for i in range(10):
      if int(player[i]) == 1:
        parity(weapons,i)
  return sum(weapons)

def parity(weapons,i):
  if weapons[i] ==  1:
    weapons[i] = 0
  weapons[i] = 1
  return weapons[i]

T = int(input())
print(weapon_value(T))
