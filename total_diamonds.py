def total_diamonds(T):
  total = []
  for i in range(T):
    N = int(input())
    total.append(number_of_diamonds(N))
  return total
def number_of_diamonds(N):
  rooms = [(i,j) for i in range(1,N+1) for j in range(1,N+1)]
  n_diamonds = []
  for x in rooms:
    even = 0
    odd = 0
    for i in range(2):
      if x[i] % 2 == 0:
        even += x[i]
      odd += x[i]
    n_diamonds.append(abs(even-odd))
  return sum(n_diamonds)
T = int(input())
print(total_diamonds(T))
