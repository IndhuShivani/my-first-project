def get_inputs():
  ncoins = int(input())
  coins = [input().split() for i in range(ncoins)]
  return ncoins,coins
def max_coins_sum():
  ncoins,coins = get_inputs()
  chefx = []
  ramsay = []
  for i in range(ncoins//2):
    chefx.append(coins.pop(i))
    ramsay.append(coins.pop(ncoins-i))
  return max(chefx),max(ramsay)
