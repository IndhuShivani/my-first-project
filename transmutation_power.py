def get_inputs():
  Ngrid = int(input())
  grid = []
  for i in range(Ngrid):
    grid.append(input().split())
  return Ngrid,grid

def transmutation_power():
  Ngrid,grid = get_inputs()
  if Ngrid > 2:
    Energies = [sum((int(grid[i][j+1]),int(grid[i+1][j]))) for i in range(Ngrid-1) for j in range(Ngrid-1)]
    return sum(Energies[::len(Energies)-1])
  return grid[0][0]

print(transmutation_power())
