def get_inputs():
  Npeople, Mlimit, Kgroups = [int(i) for i in input().split()]
  growths = [int(i) for i in input().split()]
  groups = [[int(i) for i in input().split()] for i in range(Kgroups)]
  return growths, Kgroups, groups

def growth_diff(Lowerl,Upperl,growths):
   diff = {i:growths[i-1] for i in range(Lowerl, Upperl+1)}
   return max([i-j if i != j and diff[i] == diff[j] else 0 for i in diff for j in diff])

def chef_and_growths():
  growths, Kgroups, groups = get_inputs()
  return [growth_diff(groups[x][0], groups[x][1], growths) for x in range(Kgroups)]

print(chef_and_growths())

